// 校验规则由 schema 生成，请不要直接修改当前文件，如果需要请在uniCloud控制台修改schema
// uniCloud: https://unicloud.dcloud.net.cn/



export default {
	"id": {
		"rules": [{
			"format": "int"
		}],
		"label": "编号"
	},
	"src": {
		"rules": [{
			"format": "string"
		}],
		"label": "封面图"
	},
	"title": {
		"rules": [{
				"required": true
			},
			{
				"format": "string"
			}
		],
		"defaultValue": "",
		"label": "商品名称"
	},
	"subTitle": {
		"rules": [{
			"format": "string"
		}],
		"defaultValue": "",
		"label": "子标题"
	},
	"price": {
		"rules": [{
				"required": true
			},
			{
				"format": "double"
			}
		],
		"defaultValue": 0,
		"label": "销售价格"
	},
	"originPrice": {
		"rules": [{
			"format": "double"
		}],
		"defaultValue": 0,
		"label": "原价"
	},
	"monthlySale": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 0,
		"label": "月售量"
	},
	"stock": {
		"rules": [{
				"required": true
			},
			{
				"format": "int"
			}
		],
		"defaultValue": 100,
		"label": "剩余库存"
	},
	"visite": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 0,
		"label": "浏览量"
	},
	"isSold": {
		"rules": [{
				"format": "int"
			},
			{
				"range": [{
						"text": "下架",
						"value": 0
					},
					{
						"text": "在售",
						"value": 1
					}
				]
			}
		],
		"defaultValue": 0,
		"label": "是否销售"
	},
	"posid": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 0,
		"label": "排序"
	},
	"score": {
		"rules": [{
			"format": "int"
		}],
		"defaultValue": 10,
		"label": "评分"
	}
}
