// 校验规则由 schema 生成，请不要直接修改当前文件，如果需要请在uniCloud控制台修改schema
// uniCloud: https://unicloud.dcloud.net.cn/



export default {
	"address": {
		"rules": [{
			"format": "string"
		}],
		"label": "地址"
	},
	"addressName": {
		"rules": [{
			"format": "string"
		}],
		"label": "地址简称"
	},
	"banner": {
		"rules": [{
			"format": "string"
		}],
		"label": "banner"
	},
	"currency": {
		"rules": [{
				"format": "string"
			},
			{
				"range": [{
						"text": "￥人民币",
						"value": "cny"
					},
					{
						"text": "A$澳币",
						"value": "aud"
					},
					{
						"text": "美金",
						"value": "usd"
					},
					{
						"text": "C$加拿大元",
						"value": "cad"
					},
					{
						"text": "€欧元",
						"value": "eur"
					},
					{
						"text": "£英镑",
						"value": "gbp"
					},
					{
						"text": "NZ$新西兰元",
						"value": "nzd"
					},
					{
						"text": "JP¥日元",
						"value": "jpy"
					},
					{
						"text": "₩韩元",
						"value": "krw"
					}
				]
			}
		],
		"defaultValue": "cny",
		"label": "币种"
	},
	"delivery": {
		"rules": [{
			"format": "object"
		}],
		"label": "配送方式"
	},
	"deliveryTypes": {
		"rules": [{
				"format": "array"
			},
			{
				"range": [{
						"text": "配送到家",
						"value": "deliveryHome"
					},
					{
						"text": "到店/自提点自提",
						"value": "selfRaising"
					}
				]
			}
		],
		"defaultValue": "deliveryHome",
		"label": "配送方式"
	},
	"exchangeRate": {
		"rules": [{
			"format": "double"
		}],
		"defaultValue": 1,
		"label": "汇率"
	},
	"goodsCommentTags": {
		"rules": [{
			"format": "array"
		}],
		"label": "商品评论标签"
	},
	"id": {
		"rules": [{
			"format": "int"
		}],
		"label": "店铺编号"
	},
	"isPrint": {
		"rules": [{
			"format": "bool"
		}],
		"label": "是否开启打印"
	},
	"latitude": {
		"rules": [{
			"format": "double"
		}],
		"label": "维度"
	},
	"longitude": {
		"rules": [{
			"format": "double"
		}],
		"label": "经度"
	},
	"monthSale": {
		"rules": [{
			"format": "int"
		}],
		"label": "月售"
	},
	"name": {
		"rules": [{
			"format": "string"
		}],
		"label": "店铺名称"
	},
	"notice": {
		"rules": [{
			"format": "string"
		}],
		"label": "店铺公告"
	},
	"online": {
		"rules": [{
			"format": "bool"
		}],
		"label": "支付营业"
	},
	"phone": {
		"rules": [{
			"format": "string"
		}],
		"label": "联系电话"
	},
	"score": {
		"rules": [{
			"format": "double"
		}],
		"label": "评分"
	},
	"searchGoodsKeywords": {
		"rules": [{
			"format": "array"
		}],
		"label": "商品搜索词"
	},
	"selfRaisingPoint": {
		"rules": [{
			"format": "bool"
		}],
		"defaultValue": false,
		"label": "开启自提点"
	},
	"serviceTime": {
		"rules": [{
			"format": "string"
		}],
		"label": "服务时间"
	},
	"src": {
		"rules": [{
			"format": "string"
		}],
		"label": "店铺logo"
	},
	"uid": {
		"rules": [{
			"format": "string"
		}],
		"label": "创建者"
	}
}
